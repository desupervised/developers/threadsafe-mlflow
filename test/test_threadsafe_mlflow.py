import concurrent.futures
import random
import time

import mlflow

import threadsafe_mlflow

_experiment_name = "threadsafe-mlflow-test-" + str(random.random())[5:]
print(_experiment_name)


def fn(i):
    logger = threadsafe_mlflow.create_logger(_experiment_name, "run" + i)
    for j in range(10):
        logger.log_metric("loss", float(i) * 100 + j)
    return i


def run_with_many_threads():
    n_threads = 4
    futures = []

    with concurrent.futures.ThreadPoolExecutor(max_workers=n_threads) as executor:
        for i in range(n_threads + 1):
            futures.append(executor.submit(fn, str(i)))

    concurrent.futures.wait(futures)
    print([future.result() for future in futures])


def test_running_with_many_threads():
    threadsafe_mlflow.init()
    try:
        run_with_many_threads()
    finally:
        experiment = mlflow.get_experiment_by_name(_experiment_name)
        if experiment:
            mlflow.delete_experiment(experiment.experiment_id)
