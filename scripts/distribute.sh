#!/bin/bash

if [ $# != 1 ] ; then
  echo "Usage: $0 {d2,gitlab,pypi}"
  exit 1
fi

pip install twine wheel
rm -f dist/*
python setup.py sdist bdist_wheel

if [ "$1" = "d2" ] || [ "$1" == "all" ] ; then
  python -m twine upload -u ${D2_PYPI_USERNAME} -p ${D2_PYPI_PASSWORD} --repository-url http://${D2_PYPI_HOST}:${D2_PYPI_PORT}/ dist/*
fi

if [ "$1" = "gitlab" ] || [ "$1" == "all" ] ; then
  TWINE_PASSWORD=${CI_JOB_TOKEN} TWINE_USERNAME=gitlab-ci-token python -m twine upload --repository-url ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi dist/*
fi

if [ "$1" = "pypi" ] || [ "$1" == "all" ] ; then
  # python -m twine upload -u ${D2_PYPI_USERNAME} -p ${D2_PYPI_PASSWORD} --repository-url http://${D2_PYPI_HOST}:${D2_PYPI_PORT}/ dist/*
  echo Skipping upload to PyPI, no PyPI credentials yet...
fi
