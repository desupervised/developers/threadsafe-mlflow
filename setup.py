#!/usr/bin/env python
import setuptools

if __name__ == "__main__":
    # Redirect to using setup.cfg instead. setup.py is currently still required
    # for editable installs.
    setuptools.setup()
