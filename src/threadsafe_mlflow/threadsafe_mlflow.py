"""
An `mlflow` wrapper which uses a lock to make it threadsafe.

NB this makes using `mlflow` slower, since runs are resumed and ended
upon each log.
"""
import inspect
import os
import threading

import mlflow as _mlflow
import pkg_resources

# pylint: disable=protected-access,no-member

__version__ = pkg_resources.require("threadsafe-mlflow")[0].version
_mlflow_lock = threading.Lock()
_vulnerable_functions = {
    fn_name: fn
    for fn_name, fn in inspect.getmembers(_mlflow)
    if fn_name.startswith("log") or fn_name.startswith("set")
}


def init():
    """Initialise mlflow."""
    _check_mlflow_env_vars()
    _mlflow.set_tracking_uri(os.getenv("MLFLOW_TRACKING_URI"))


def create_logger(experiment_name, run_name):
    """Create a threadsafe mlflow logger.

    The logger has all the same `{log,set}_*` methods as the regular
    `mlflow` module, except upon each call to these methods, the global
    context is updated to ensure that `experiment_name` and `run_name`
    are the current scope.
    """
    mutexed_logger = type(
        "mutexed_logger",
        (),
        {
            fn_name: _create_threadsafe_fn(fn, experiment_name, run_name)
            for fn_name, fn in _vulnerable_functions.items()
        },
    )
    mutexed_logger._run_id = None
    return mutexed_logger


def _check_mlflow_env_vars():
    for env_name in [
        "AWS_DEFAULT_REGION",
        "AWS_ACCESS_KEY_ID",
        "AWS_SECRET_ACCESS_KEY",
        "MLFLOW_S3_ENDPOINT_URL",
        "MLFLOW_SERVER_DEFAULT_ARTIFACT_ROOT",
        "MLFLOW_TRACKING_USERNAME",
        "MLFLOW_TRACKING_PASSWORD",
        "MLFLOW_TRACKING_URI",
    ]:
        if not os.getenv(env_name):
            msg = f"Environment variable '{env_name}' required when MLFLOW_ENABLED=1"
            raise RuntimeError(msg)


def _get_or_create_experiment(experiment_name):
    with _mlflow_lock:
        experiment = _mlflow.get_experiment_by_name(experiment_name)
        if experiment is None:
            experiment_id = _mlflow.create_experiment(experiment_name)
        else:
            experiment_id = experiment.experiment_id
        return experiment_id


def _create_run(experiment_id, run_name):
    with _mlflow.start_run(run_name=run_name, experiment_id=experiment_id) as run:
        return run.info.run_id


def _create_threadsafe_fn(log_fn, experiment_name, run_name):
    experiment_id = _get_or_create_experiment(experiment_name)

    @classmethod
    def _safe_fn(cls, *args, **kwargs):
        with _mlflow_lock:
            if cls._run_id is None:
                cls._run_id = _create_run(experiment_id, run_name)
            with _mlflow.start_run(run_id=cls._run_id):
                log_fn(*args, **kwargs)

    return _safe_fn
