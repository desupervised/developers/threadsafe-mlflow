"""
Init module.
"""

from threadsafe_mlflow.threadsafe_mlflow import create_logger, init
