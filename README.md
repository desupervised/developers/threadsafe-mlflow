# Threadsafe MLFlow

A thin wrapper around `MLFlow` which is threadsafe while logging.

## Usage

To enable logging, the environment variable `MLFLOW_ENABLED` must be set to `1`.
See below for the other required environment variables.

A basic example is as follows:

```py
import threadsafe_mlflow

def train():
    logger = threadsafe_mlflow.create_logger(experiment_name="myexperiment", run_name="myrun")
    for i in epochs:
        loss = ...
        logger.log_metric("loss", loss, step=i)
```

## Required Environment Variables

The following environment variables are required when this package is enabled:

- `AWS_DEFAULT_REGION`
- `AWS_ACCESS_KEY_ID`
- `AWS_SECRET_ACCESS_KEY`
- `MLFLOW_S3_ENDPOINT_URL`
- `MLFLOW_SERVER_DEFAULT_ARTIFACT_ROOT`
- `MLFLOW_TRACKING_USERNAME`
- `MLFLOW_TRACKING_PASSWORD`
- `MLFLOW_TRACKING_URI`
